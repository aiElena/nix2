<?php

namespace AlexIndustry\Framework\Models;

use AlexIndustry\Framework\DB;

abstract class Model
{
    /**
     * @var DB
     */
    protected $pdo;
    protected string $table;

    protected $created_at = null;
    protected $updated_at = null;

    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table";
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @return array
     */
    public function where($search, $operator, $id): array
    {
        $sql = "SELECT * FROM $this->table WHERE $search $operator '$id'";
        return $this->pdo->query($sql);
    }

    /**
     * @param $array
     * @return array
     */
    public function insert($array): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->updated_at) {
            $arr = [
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ];
            $array = array_merge($array, $arr);
        }

        $keysString = implode(',', array_keys($array));
        $valuesString = implode('\',\'', array_values($array));

        $sql = "INSERT INTO $this->table ($keysString) VALUES ('$valuesString')";

        return $this->pdo->query($sql);
    }
}
