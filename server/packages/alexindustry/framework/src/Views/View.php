<?php

namespace AlexIndustry\Framework\Views;
/**
 * Class View.
 *
 * @author Alexandrova <ai.alexandrova@ukr.net>
 */
class View
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * @var string
     */
    public string $view;

    /**
     * @param array $route
     * @param string $view
     */
    public function __construct(array $route, string $view = '')
    {
        $this->route = $route;
        $this->view = $view;
    }

    /**
     * @param $data
     * @return void
     */
    public function render($data):void
    {
        $fileView = ROOT . '/App/Views/' . $this->view . '.php';
        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo '<h1>Файл не найден! ' . $fileView . '</h1>';
        }
    }
}