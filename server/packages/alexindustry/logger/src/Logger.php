<?php

namespace AlexIndustry\Logger;


class Logger
{
    public static function log($context)
    {
        self::method(require 'config/log.php')->writelog(__FUNCTION__, $context);
    }
    public static function error($context)
    {
        self::method(require 'config/log.php')->writelog(__FUNCTION__, $context);
    }

    public static function method($class)
    {
        $listenerClass = 'AlexIndustry\\Logger\\Methods\\' . ucfirst($class) . 'ClassFactory';
        if (class_exists($listenerClass)) {
            //$log = static::getInstance();
            return $listenerClass::getInstance();
        } else {
            //echo $listenerClass;
            exit('This class does not exist!' . ucfirst($class) . 'ClassFactory');
        }
    }
}