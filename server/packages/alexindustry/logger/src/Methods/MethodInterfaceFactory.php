<?php

namespace AlexIndustry\Logger\Methods;

interface MethodInterfaceFactory
{
    public function writelog($level, $message);
}