<?php

namespace App\Models;

use AlexIndustry\Framework\Models\Model;

class ArticleModel extends Model
{
    protected string $table = 'articles';
}