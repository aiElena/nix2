<?php

namespace App\Models;

use AlexIndustry\Framework\Models\Model;

class UserModel extends Model
{
    protected string $table = 'users';

    protected $created_at = true;
    protected $updated_at = true;

    public static function created($array)
    {
        $userModel = new self;
        $userModel->insert($array);
    }
    public static function getOne($id)
    {
        $userModel = new self;
        $res = $userModel->where('id', '=', $id);
        return $res[0]; //first() = limit 1
    }
}