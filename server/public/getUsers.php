<?php
try {
    $options = [
       PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
       PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
       // PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    ];
    $dbh = new PDO('mysql:host=nix_2_mysql;port=3306;dbname=nix2', 'myapp', 'myapp', $options);

    $js = json_encode($dbh->query('SELECT * FROM users')->fetchAll());
    echo $js;
    return $js;
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}